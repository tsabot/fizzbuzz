const { ruleFizz, ruleBuzz, buzzFizzRule } = require("./rule");
const { Strategy } = require("./strategy");

const division = new Strategy([ruleFizz, ruleBuzz]);

division.compute = (number) => {
  return division.rules.reduce((messages, rule) => {
    const addOutPut = number % rule.input === 0;

    if (addOutPut) {
      return [...messages, rule.output];
    }

    return messages;
  }, []);
};

const contains = new Strategy([ruleFizz, ruleBuzz]);

contains.compute = (number) => {
  return division.rules.reduce((messages, rule) => {
    const addOutPut = number.toString().search(rule.input.toString()) > -1;

    if (addOutPut) {
      return [...messages, rule.output];
    }

    return messages;
  }, []);
};

const fixMessage = new Strategy([buzzFizzRule]);

fixMessage.compute = (message) => {
  return fixMessage.rules.reduce((fixedMessage, rule) => {
    const searchInput = message.search(rule.input) > -1;

    if (searchInput > -1) {
      return fixedMessage.replace(rule.input, rule.output);
    }

    return fixedMessage;
  }, message);
};

module.exports = {
  division,
  contains,
  fixMessage,
};
