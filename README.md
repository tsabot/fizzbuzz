# FizzBuzz Solution

This project is a solution of the Dojo :
https://codingdojo.org/kata/FizzBuzz/

## Introduction

### About this Kata

This Kata was posted here by someone anonymously. Michael Feathers and EmilyBache performed it at agile2008 when competing in “Programming with the stars” in python, in 4 minutes.

Difficulty: Easy Good for teaching: TDD , BabySteps

### Problem Description

Imagine the scene. You are eleven years old, and in the five minutes before the end of the lesson, your Maths teacher decides he should make his class more “fun” by introducing a “game”. He explains that he is going to point at each pupil in turn and ask them to say the next number in sequence, starting from one. The “fun” part is that if the number is divisible by three, you instead say “Fizz” and if it is divisible by five you say “Buzz”. So now your maths teacher is pointing at all of your classmates in turn, and they happily shout “one!”, “two!”, “Fizz!”, “four!”, “Buzz!”… until he very deliberately points at you, fixing you with a steely gaze… time stands still, your mouth dries up, your palms become sweatier and sweatier until you finally manage to croak “Fizz!”. Doom is avoided, and the pointing finger moves on.

So of course in order to avoid embarassment infront of your whole class, you have to get the full list printed out so you know what to say. Your class has about 33 pupils and he might go round three times before the bell rings for breaktime. Next maths lesson is on Thursday. Get coding!

Write a program that prints the numbers from 1 to 100. But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”. For numbers which are multiples of both three and five print “FizzBuzz “.

Sample output:

```
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
```

### Step 2:

```
* A number is fizz if it is divisible by 3 or if it has a 3 in it
* A number is buzz if it is divisible by 5 or if it has a 5 in it
```

## How it happend

Two steps occured :

- It as been made with TDD initialy in functionnal JS. [link](https://gitlab.com/tsabot/fizzbuzz/-/tree/397af4c43641b8f6e84527c59edc61707d58edce)
- It as been re-made, keeping the tests, enabling them one by one in OOP JS with a more abstract approach to the solution.

Please keep in mind that it was a timed exercice

## How to run

Install Node.js v12 or higher

Install dependency:

```bash
npm i
```

To run the project:

```bash
npm run start
```

To run Tests:

```bash
npm run test
```
