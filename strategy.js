class Strategy {
  rules;

  constructor(rules) {
    this.rules = rules;
  }

  compute = () => {};
}

module.exports = {
  Strategy,
};
