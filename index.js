const { division, contains, fixMessage } = require("./strategyImplementations");

class Game {
  numberOfChilds;

  constructor(numberOfChilds) {
    this.numberOfChilds = numberOfChilds;
  }

  computeChildMessage = (number) =>
    [
      ...new Set([...division.compute(number), ...contains.compute(number)]),
    ].join("");

  getChildMessage = (number) => {
    const message = this.computeChildMessage(number);

    return message ? fixMessage.compute(message) : `${number}`;
  };

  run = () => {
    for (
      let childNumber = 0;
      childNumber < this.numberOfChilds;
      childNumber++
    ) {
      const message = this.getChildMessage(childNumber);

      console.log(message);
    }
  };
}

const fizzBuzzGame = new Game(100);

module.exports = {
  Game,
  fizzBuzzGame,
};

fizzBuzzGame.run();
