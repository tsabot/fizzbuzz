const { fizzBuzzGame } = require("./index");

const consoleLogMock = jest.spyOn(console, "log").mockImplementation();

afterAll(() => {
  consoleLogMock.mockRestore();
});

describe("Test Fizz Buzz", () => {
  it("Should return 1 to string", () => {
    expect(fizzBuzzGame.getChildMessage(1)).toBe("1");
  });
  it("Should return Fizz if number is divisible by 3", () => {
    expect(fizzBuzzGame.getChildMessage(3)).toBe("Fizz");
  });
  it("Should return 4 to string", () => {
    expect(fizzBuzzGame.getChildMessage(4)).toBe("4");
  });
  it("Should return Buzz if number is divisible by 5", () => {
    expect(fizzBuzzGame.getChildMessage(5)).toBe("Buzz");
  });
  it("Should return Buzz if number is divisible by 5 and 3", () => {
    expect(fizzBuzzGame.getChildMessage(15)).toBe("FizzBuzz");
  });
  it("Should execute fizzBuzz 100 times", () => {
    fizzBuzzGame.run();
    expect(consoleLogMock).toHaveBeenCalledTimes(100);
  });
  it("Should return Fizz if number contains 3", () => {
    expect(fizzBuzzGame.getChildMessage(13)).toBe("Fizz");
  });
  it("Should return Buzz if number contains 5", () => {
    expect(fizzBuzzGame.getChildMessage(52)).toBe("Buzz");
  });
  it("Should return FizzBuzz if number contains 5 and 3", () => {
    expect(fizzBuzzGame.getChildMessage(53)).toBe("FizzBuzz");
  });
  it("Should return FizzBuzz if number contains 3 and 5", () => {
    expect(fizzBuzzGame.getChildMessage(305)).toBe("FizzBuzz");
  });
});
