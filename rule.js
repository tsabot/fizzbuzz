class Rule {
  input;
  output;

  constructor(input, output) {
    this.input = input;
    this.output = output;
  }
}

const ruleFizz = new Rule(3, "Fizz");
const ruleBuzz = new Rule(5, "Buzz");
const buzzFizzRule = new Rule("BuzzFizz", "FizzBuzz");

module.exports = {
  Rule,
  ruleFizz,
  ruleBuzz,
  buzzFizzRule,
};
